### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

# Harrison's Java Spring Boot Application
Java Spring Boot application to offer an API to use alongside Slack's slash commands to implement a full CI/CD pipeline using GitLab.

The pipeline is separated into the following stages

*Build:* The Spring Boot application is compiled and artifacts created

*Test:* Unit tests are run against the code to ensure everything is working properly

*Upload Artifacts:* The jars generated from build are sent to S3, which stores artifacts

*Deploy:* The artifacts are used to deploy the application
